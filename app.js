'use strict';

const uploadFile = document.getElementById('upload-file');
const video = document.getElementById('video');
const loader = document.querySelector('.loading');
const controls = document.querySelector('.controls');
const playBtn = document.querySelector('.playBtn');
const playIcon = document.querySelector('.playIcon');
const pauseIcon = document.querySelector('.pauseIcon');
const stopBtn = document.querySelector('.stopBtn');
const volumeRange = document.querySelector('.volumeRange');
const fullScreenBtn = document.querySelector('.fullScreenBtn');

if (window.File && window.FileReader && window.FileList) {
  console.log('Todas las APIs soportadas.');
} else {
  alert('La API de FILE no es soportada en este navegador.');
};

function handleFile(e) {
  let file = e.target.files[0];
  let type = file.type;
 
  let reader = new FileReader();

  if (type.includes('video')) {

    reader.onloadstart = () => {
      loader.classList.remove('hide');
    }

    reader.onload = (e) => {
      let uploadedVideo = e.target.result;
     
      video.setAttribute('src', uploadedVideo);
    } 

    reader.onloadend = (e) => {
      let uploadedVideo = e.target.result;

      if (uploadedVideo) {
        loader.classList.add('hide');
        controls.style.transform = 'translatey(0px)';
        video.style.minWidth = 'auto';
      }
    };
    reader.readAsDataURL(file);
  } else {
    alert('Solo archivos de tipo vídeo soportados.')
  }  
};

uploadFile.addEventListener('change', handleFile);

video.addEventListener("click", () => !video.paused ? pauseVideo() : playVideo());

playBtn.addEventListener("click", () => !video.paused ? pauseVideo() : playVideo());

stopBtn.addEventListener("click", () => {
  video.pause();
  video.currentTime = 0;
  pauseIcon.classList.add('hide');
  playIcon.classList.remove('hide');
});

volumeRange.addEventListener('input', () => video.volume = volumeRange.value);

function playVideo() {
  playBtn.setAttribute('aria-label', 'Pausar');
  video.play();
  pauseIcon.classList.remove('hide');
  playIcon.classList.add('hide');
};

function pauseVideo() {
  playBtn.setAttribute('aria-label', 'Reproducir');
  video.pause();
  pauseIcon.classList.add('hide');
  playIcon.classList.remove('hide');
};

fullScreenBtn.addEventListener('click', () => {
  const safari = navigator.userAgent.indexOf("Safari") > -1;

  !safari ? video.requestFullscreen() : video.webkitRequestFullScreen();
});